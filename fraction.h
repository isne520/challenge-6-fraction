// a header file ( interface )
#include <iostream>
using namespace std;
#ifndef FRACTION_H
#define FRACTION_H
class fraction
{
public:
	fraction(int newnum, int newdenom) // constructors
	{
		num = newnum;
		denom = newdenom;
	}
	void setnum(int newnum) // mutators
	{
		num = newnum;
	}
	void setdenom(int newdenom) // mutators
	{
		denom = newdenom;
	}
	int getnum() //accessor
	{
		return num;
	}
	int getdenom() //accessor
	{
		return denom;
	}
	friend istream &operator >> (istream &input, fraction &newf);
	friend ostream &operator <<(ostream &output, fraction &newf);
	friend fraction operator+(fraction &newf1, fraction &newf2);
	friend fraction operator-(fraction &newf1, fraction &newf2);
	friend fraction operator*(fraction &newf1, fraction &newf2);
	friend fraction operator/(fraction &newf1, fraction &newf2);
	friend fraction operator++(fraction &newf);
	friend fraction operator--(fraction &newf);
	friend bool operator==(fraction &newf1, fraction &newf2);
	friend bool operator!=(fraction &newf1, fraction &newf2);
	friend bool operator>(fraction &newf1, fraction &newf2);
	friend bool operator<(fraction &newf1, fraction &newf2);
	friend bool operator>=(fraction &newf1, fraction &newf2);
	friend bool operator<=(fraction &newf1, fraction &newf2);
private:
	int num;
	int denom;
};
#endif