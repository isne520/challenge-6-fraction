// implementation file
#include <iostream>
#include<cmath>
#include "fraction.h"
using namespace std;
istream &operator >> (istream &input, fraction &newf)
{
	input >> newf.num >> newf.denom;
	return input;
}
ostream &operator<< (ostream &output, fraction &newf)
{
	output << newf.num << "/" << newf.denom;
	return output;
}
fraction operator+ (fraction &newf1, fraction &newf2)
{
	fraction newf3((newf1.num * newf2.denom) + (newf2.num * newf1.denom), newf1.denom * newf2.denom);
	return newf3;
}
fraction operator- (fraction &newf1, fraction &newf2)
{
	fraction newf4((newf1.num * newf2.denom) - (newf2.num * newf1.denom), newf1.denom * newf2.denom);
	return newf4;
}
fraction operator* (fraction &newf1, fraction &newf2)
{
	fraction newf5((newf1.num * newf2.num), (newf1.denom * newf2.denom));
	return newf5;
}
fraction operator/ (fraction &newf1, fraction &newf2)
{
	fraction newf6((newf1.num * newf2.denom), (newf1.denom * newf2.num));
	return newf6;
}
fraction operator++ (fraction &newf)
{
	fraction newfn((newf.num + newf.denom), newf.denom);
	return newfn;
}
fraction operator-- (fraction &newf)
{
	fraction newfn((newf.num - newf.denom), newf.denom);
	return newfn;
}
bool operator== (fraction &newf1, fraction &newf2)
{
	if (newf1.num == newf2.num && newf1.denom == newf2.denom) // num f1 = num f2 and denom f1 = denom f2
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool operator!= (fraction &newf1, fraction &newf2)
{
	if (newf1.num != newf2.num && newf1.denom != newf2.denom) // num f1 != num f2 and denom f1 != denom f2
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool operator>(fraction &newf1, fraction &newf2)
{
	fraction newwf1((newf1.num * newf2.denom), (newf1.denom * newf2.denom));
	fraction newwf2((newf2.num * newf1.denom), (newf1.denom * newf2.denom));
	if (newwf1.num > newwf2.num)
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool operator<(fraction &newf1, fraction &newf2)
{
	fraction newwf1((newf1.num * newf2.denom), (newf1.denom * newf2.denom));
	fraction newwf2((newf2.num * newf1.denom), (newf1.denom * newf2.denom));
	if (newwf1.num < newwf2.num)
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool operator>=(fraction &newf1, fraction &newf2)
{
	fraction newwf1((newf1.num * newf2.denom), (newf1.denom * newf2.denom));
	fraction newwf2((newf2.num * newf1.denom), (newf1.denom * newf2.denom));
	if (newwf1.num >= newwf2.num)
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool operator<=(fraction &newf1, fraction &newf2)
{
	fraction newwf1((newf1.num * newf2.denom), (newf1.denom * newf2.denom));
	fraction newwf2((newf2.num * newf1.denom), (newf1.denom * newf2.denom));
	if (newwf1.num <= newwf2.num)
	{
		return true;
	}
	else
	{
		return false;
	}
}